# ActionSocket

ActionSocket is a (mostly) stateless websocket server that provides a generic backend to rich multi-session client applications.

Applications are often built as backend services that serve a client - be it a mobile phone, a web browser or some other device or interface. These backends manage application state and update each client when that state is changed. Instead of splitting applications in this way, is it possible to allow clients to own much more of the application logic and keep the server as lightweight and generic as possible? Could one generic server serve many different types of application?

ActionSocket is a lightweight communication hub that enables client applications to create connections with each other and share state without implementing any application state managment on the server itself; instead the client is responsible for everything the application does and only uses the server to communicate between clients.


## Design Principles

AcitonSocket is designed to be:

 1. __Generic__: provides a reusable websocket backend for many different client applications on the same server
 2. __Scalable__: has the ability to serve a large number of concurrent connections
 3. __Stateless__: should pass responsibilty for managing application state to the client only holding internal state where necessary
 4. __Event Sourced__: allows client application to recreate state by replaying a log of events
 5. __Deterministic__: enables client applications to procedurally generate state through client seeded pseudorandom number generation
 6. __Consumer Focused__: has a well documented, stable and consisten API and is simple to build and deploy


## API

### Sessions and new connections

A session is a collection of connections which can send messages to each other. When a new connection is opened on a session, that sessions event log will be sent to that connection.

A session can be joined by establishing a websocket connection to `/socket/{sessionId}` where `{sessionId}` is the identifier for that connection. Session identifiers can be any alphanumeric string. Connecting to a session that does not exist will create a new session. When the last connection to a session is closed, that session will be destroyed.

Sessions have a maximum number of connections which is currently set to 8. After this limit has been reached, attempts to connect will be rejected.

### Messages

All messages require an `action` and a `method` and can have an optional `payload`. Some methods require other parameters to be specified and these are documented in the section for each `method`.

An `action` is simply a string value that represents an action that has been performed by the sender. This value is essentially acts as a message type which the client can use when deciding how to handle that message. It is conventional to represent an `action` as an uppercase snake case (a.k.a "SCREAMING_SNAKE_CASE") short value in the past tense, for example: `USERNAME_SET` and `PLAYER_JOINED`.

A `method` specifies what kind of message is being sent and who it will be sent to. `method` must be one of the following:

  * [BROADCAST](#BROADCAST)
  * [BROADCAST_OUT](#BROADCAST_OUT)
  * [DIRECT](#DIRECT)
  * [CONSENSUS](#CONSENSUS)
  * [COLLATE](#COLLATE)
  * [COLLATE_AND_DROP_ONE](#COLLATE_AND_DROP_ONE) 
  * [QUORUM](#QUORUM)

### <a name="BROADCAST"></a>BROADCAST
  Send a message to every connection in the session

### <a name="BROADCAST_OUT"></a>BROADCAST_OUT
  Send a message to every connection in this session except the sender

### <a name="DIRECT"></a>DIRECT
  Send a message to one or more specified connections

### <a name="CONSENSUS"></a>CONSENSUS
  Once the server has recieved the same message from all connections, send that message out to all connections

### <a name="COLLATE"></a>COLLATE
  Wait for all connections to send the same message with potentially different payloads and then send that message to all connections with a randomly ordered list of payloads from each sender.

### <a name="COLLATE_AND_DROP_ONE"></a>COLLATE_AND_DROP_ONE
  Like collate except one sender's payload will be dropped at random from the final message

### <a name="QUORUM"></a>QUORUM
  Once the server has recieved the same message from a set number of connections, send that message out to all connections.

# TODOs:

  * Allow sessions to be locked with a password
