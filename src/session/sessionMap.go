package session

import (
	"math/rand"
	"message"
)

type SessionMap map[string]*Session

func (sm SessionMap) createSession(id string) *Session {

	sm[id] = &Session{
		id:                id,
		seed:              rand.Int(),
		consensusMessages: make(map[Checksum]connectionSet),
		quorumMessages:    make(map[string]connectionSet),
		collationMessages: make(map[string]payloadSet),
		connections:       make(ConnectionMap, 0),
		eventLog:          make([]*message.Output, 0),
	}
	return sm[id]
}

func (sm SessionMap) JoinSession(sessionId string) *Session {
	session := sm[sessionId]
	if session == nil {
		session = sm.createSession(sessionId)
	}
	return session
}

func (sm SessionMap) CleanUpSession(id string) {
	session := sm[id]
	if len(session.connections) == 0 {
		delete(sm, id)
	}
}
