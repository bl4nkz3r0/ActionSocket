package session

import (
	"config"
	"connection"
	"context"
	"crypto/md5"
	"errors"
	"fmt"
	"math/rand"
	"message"

	"github.com/gorilla/websocket"
)

type Checksum [16]byte

type Session struct {
	id                string
	seed              int
	connections       ConnectionMap
	consensusMessages map[Checksum]connectionSet
	quorumMessages    map[string]connectionSet
	collationMessages map[string]payloadSet
	eventLog          []*message.Output
}

type payloadSet map[string]message.PayloadObject

type connectionSet map[string]bool

type ConnectionMap map[string]*connection.Connection

func (s *Session) Connect(socket *websocket.Conn, ctx context.Context) (error, *connection.Connection) {
	if len(s.connections) >= config.MAX_PLAYERS {
		err := errors.New("Session full - can not connect.")
		return err, nil
	}
	connectionPtr := connection.CreateConnection(socket, ctx)
	s.connections[connectionPtr.Id] = connectionPtr
	return nil, connectionPtr
}

func (s *Session) EventLog() []message.Output {
	events := make([]message.Output, 0, len(s.eventLog))
	for _, e := range s.eventLog {
		events = append(events, *e)
	}
	return events
}

func (s *Session) BroadcastAll(msg *message.Output) {
	for _, c := range s.connections {
		go s.Send(c.Id, msg)
	}
	s.eventLog = append(s.eventLog, msg)
}

func (s *Session) SendConnectMessage(connectionId string) {
	msg := message.NewConnectionMsg(connectionId, "CONNECTED")
	var ids []string
	for _, c := range s.connections {
		if c.Id != connectionId {
			ids = append(ids, c.Id)
		}
	}
	s.SendToMany(ids, msg)
	s.eventLog = append(s.eventLog, msg)
}

func (s *Session) SendDisconnectMessage(c *connection.Connection) {
	msg := message.NewConnectionMsg(c.Id, "DISCONNECTED")
	delete(s.connections, c.Id)
	go s.Broadcast(msg)
	s.eventLog = append(s.eventLog, msg)
}

func (s *Session) Broadcast(msg *message.Output) {
	for _, c := range s.connections {
		if c.Id == msg.Sender {
			continue
		}
		go s.Send(c.Id, msg)
	}
	s.eventLog = append(s.eventLog, msg)
}

func (s *Session) BroadcastOnCollation(connectionId string, input *message.Input, skip int) {
	key := fmt.Sprint(input.Seed) + input.Action
	if s.collationMessages[key] == nil {
		s.collationMessages[key] = make(payloadSet)
	}
	collationPayloadMap := s.collationMessages[key]
	collationPayloadMap[connectionId] = input.Payload

	connectionsLen := len(collationPayloadMap)
	if connectionsLen >= len(s.connections) {
		skipPerm := rand.Perm(connectionsLen)[0:skip]
		msg := input.ToCollatedOutput()
		i := -1
	mainLoop:
		for _, payload := range collationPayloadMap {
			i++
			for _, n := range skipPerm {
				if n == i {
					continue mainLoop
				}
			}
			msg.CollatedPayload = append(msg.CollatedPayload, payload)
		}
		for toId, _ := range collationPayloadMap {
			conn := s.connections[toId]
			if conn != nil {
				s._send(conn, msg)
			}
		}
		delete(s.collationMessages, key)
	}
}

func (s *Session) BroadcastOnQuorum(connectionId string, input *message.Input, rawBytes []byte, quorum int) {
	// TODO: key needs to include full message to ensure that the message isn't changed by the final quorum message sender - perhaps use checksum instead
	key := fmt.Sprint(input.Seed) + input.Action
	// find or create a connectionSet
	if s.quorumMessages[key] == nil {
		s.quorumMessages[key] = make(connectionSet)
	}
	quorumConnections := s.quorumMessages[key]

	// add connection to set
	quorumConnections[connectionId] = true

	// when all connections have reached quorum
	if len(quorumConnections) >= quorum {
		// create a new seed
		randSource := rand.NewSource(int64(input.Seed))
		nextSeed := rand.New(randSource).Int()
		msg := input.ToSeededOutput(nextSeed)
		for _, conn := range s.connections {
			s._send(conn, msg)
		}
		// add message to eventLog
		s.eventLog = append(s.eventLog, msg)
		// remove connectionSet from session quorumMessages
		delete(s.quorumMessages, key)
	} else {
		s.Broadcast(message.NewPendingActionMsg(connectionId, input.Action))
	}
}

func (s *Session) BroadcastOnConsensus(connectionId string, input *message.Input, rawBytes []byte) {
	checksum := md5.Sum(rawBytes)
	// find or create a connectionSet
	if s.consensusMessages[checksum] == nil {
		s.consensusMessages[checksum] = make(connectionSet)
	}
	consensusConnections := s.consensusMessages[checksum]

	// add connection to set
	consensusConnections[connectionId] = true

	// when all connections have reached consensus
	if len(consensusConnections) >= len(s.connections) {
		// create a new seed
		randSource := rand.NewSource(int64(input.Seed))
		nextSeed := rand.New(randSource).Int()
		msg := input.ToSeededOutput(nextSeed)
		// send msg to all connections
		for _, conn := range s.connections {
			s._send(conn, msg)
		}
		// add message to eventLog
		s.eventLog = append(s.eventLog, msg)
		// remove connectionSet from session consensusMessages
		delete(s.consensusMessages, checksum)
	} else {
		s.Broadcast(message.NewPendingActionMsg(connectionId, input.Action))
	}
}

func (s *Session) SendToMany(ids []string, msg *message.Output) {
	for _, to := range ids {
		go s.Send(to, msg)
	}
}

func (s *Session) SendInit(connectionId string) {
	msg := message.InitMsg(connectionId, s.seed, s)
	s.Send(connectionId, msg)
}

func (s *Session) Send(toId string, msg *message.Output) {
	to := s.connections[toId]
	if to == nil {
		err := fmt.Errorf("Connection %q not found", toId)
		o := message.NewError(err.Error())
		to := s.connections[msg.Sender]
		s._send(to, o)
	}
	s._send(to, msg)
}

func (s *Session) _send(to *connection.Connection, msg *message.Output) {
	if to == nil {
		return
	}
	select {
	case <-to.Ctx.Done():
	case to.Msg <- msg:
	}
}
