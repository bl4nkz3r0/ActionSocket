package main

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"message"
	"randomName"
	"session"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var AllSessions session.SessionMap = make(session.SessionMap)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	// allow all connections for development
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func socketConnection(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sessionId := vars["sessionId"]
	session := AllSessions.JoinSession(sessionId)
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	err, connection := session.Connect(conn, ctx)
	if err != nil {
		msg := message.NewError(err.Error())
		conn.WriteJSON(msg)
		conn.Close()
		return
	}

	go connection.Subscribe(session, cancel)
	go connection.Publish(session, cancel)

	go session.SendInit(connection.Id)
	go session.SendConnectMessage(connection.Id)

	go func() {
		<-ctx.Done()
		conn.Close()
		session.SendDisconnectMessage(connection)
		AllSessions.CleanUpSession(sessionId)
	}()
}

func pageHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	joinQueryParam := q.Get("join")
	var sessionId string
	if len(joinQueryParam) >= 3 {
		sessionId = joinQueryParam
	} else {
		sessionId = randomName.Generate(3)
	}
	if joinQueryParam != sessionId {
		http.Redirect(w, r, "/?join="+sessionId, 303)
	}
	t, err := template.ParseFiles("templates/index.html")
	if err != nil {
		log.Println(err)
		return
	}
	err = t.ExecuteTemplate(w, "index.html", sessionId)
	if err != nil {
		log.Println(err)
		return
	}
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", pageHandler)
	r.HandleFunc("/socket/{sessionId}", socketConnection)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	fmt.Println("Server listening at 0.0.0.0:8080")
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", r))
}
