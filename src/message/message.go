package message

import "time"

type PayloadObject map[string]interface{}

type Output struct {
	Action          string          `json:"action"`
	Sender          string          `json:"sender"`
	SentAt          int64           `json:"sentAt"`
	Seed            uint32          `json:"seed,omitempty"`
	Payload         PayloadObject   `json:"payload,omitempty"`
	CollatedPayload []PayloadObject `json:"collatedPayload,omitempty"`
}

type Clock interface {
	NowInMilliseconds() int64
}

type Input struct {
	Method    string        `json:"method"`
	Addresses []string      `json:"addresses,omitempty"`
	Action    string        `json:"action"`
	Seed      int           `json:"seed,omitempty"`
	Quorum    int           `json:"quorum,omitempty"`
	Payload   PayloadObject `json:"payload"`
}

type eventLogger interface {
	EventLog() []Output
}

type clock struct{}

func (clock) NowInMilliseconds() int64 {
	return time.Now().UnixNano() / 1e6
}

func NowInMilliseconds(clockOverrides []Clock) int64 {
	var c Clock
	if len(clockOverrides) == 1 {
		c = clockOverrides[0]
	} else {
		c = clock{}
	}
	return c.NowInMilliseconds()
}

func (i Input) ToOutput(sender string, clockOverride ...Clock) *Output {
	return &Output{
		Sender:  sender,
		SentAt:  NowInMilliseconds(clockOverride),
		Action:  i.Action,
		Payload: i.Payload,
	}
}

func (i Input) ToCollatedOutput(clockOverride ...Clock) *Output {
	return &Output{
		Sender:          "APPLICATION",
		SentAt:          NowInMilliseconds(clockOverride),
		Action:          i.Action,
		CollatedPayload: make([]PayloadObject, 0),
	}
}

func (i Input) ToSeededOutput(newSeed int, clockOverride ...Clock) *Output {
	return &Output{
		Sender:  "APPLICATION",
		SentAt:  NowInMilliseconds(clockOverride),
		Seed:    uint32(newSeed),
		Action:  i.Action,
		Payload: i.Payload,
	}
}

func New(o Output, clockOverride ...Clock) *Output {
	return &Output{
		SentAt:  NowInMilliseconds(clockOverride),
		Sender:  o.Sender,
		Action:  o.Action,
		Payload: o.Payload,
	}
}

func NewError(errorMessage string, clockOverride ...Clock) *Output {
	return &Output{
		Sender: "APPLICATION",
		SentAt: NowInMilliseconds(clockOverride),
		Action: "ERROR",
		Payload: PayloadObject{
			"error": errorMessage,
		},
	}
}

func NewConnectionMsg(sender, action string, clockOverride ...Clock) *Output {
	return &Output{
		Sender: "APPLICATION",
		SentAt: NowInMilliseconds(clockOverride),
		Action: action,
		Payload: PayloadObject{
			"connectionId": sender,
		},
	}
}

func NewPendingActionMsg(sender, action string, clockOverride ...Clock) *Output {
	return &Output{
		Sender: "APPLICATION",
		SentAt: NowInMilliseconds(clockOverride),
		Action: "PENDING_ACTION",
		Payload: PayloadObject{
			"pending": PayloadObject{
				"action": action,
				"sender": sender,
			},
		},
	}
}

func InitMsg(sender string, seed int, e eventLogger, clockOverride ...Clock) *Output {
	return &Output{
		Sender: "APPLICATION",
		SentAt: NowInMilliseconds(clockOverride),
		Seed:   uint32(seed),
		Action: "INIT",
		Payload: PayloadObject{
			"connectionId": sender,
			"eventLog":     e.EventLog(),
		},
	}
}
