package message

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type _clockStub struct{}

func (_clockStub) NowInMilliseconds() int64 {
	return 1234567654321
}

type _eventLoggerStub struct{}

func (_eventLoggerStub) EventLog() []Output {
	return []Output{
		Output{
			Action: "ONE",
			SentAt: 0000000000001,
			Sender: "sender1",
		},
		Output{
			Action: "TWO",
			SentAt: 0000000000002,
			Sender: "sender1",
		},
	}
}

func TestInputMethods(t *testing.T) {
	clockStub := _clockStub{}
	payload := PayloadObject{
		"one":   1,
		"two":   2,
		"three": 3,
	}

	input := Input{
		Method:  "BROADCAST",
		Action:  "ONE",
		Seed:    1,
		Payload: payload,
	}

	t.Run("ToOutput", func(t *testing.T) {
		expected := &Output{
			Action:  "ONE",
			SentAt:  1234567654321,
			Sender:  "sender1",
			Payload: payload,
		}
		actual := input.ToOutput("sender1", clockStub)
		assert.Equal(t, expected, actual)
	})

	t.Run("ToCollatedOutput", func(t *testing.T) {
		expected := &Output{
			Action:          "ONE",
			SentAt:          1234567654321,
			Sender:          "APPLICATION",
			CollatedPayload: make([]PayloadObject, 0),
		}
		actual := input.ToCollatedOutput(clockStub)
		assert.Equal(t, expected, actual)
	})

	t.Run("ToSeededOutput", func(t *testing.T) {
		expected := &Output{
			Action:  "ONE",
			SentAt:  1234567654321,
			Sender:  "APPLICATION",
			Seed:    uint32(5),
			Payload: payload,
		}
		actual := input.ToSeededOutput(5, clockStub)
		assert.Equal(t, expected, actual)
	})
}

func TestOutputGenerators(t *testing.T) {
	clockStub := _clockStub{}
	payload := PayloadObject{
		"one":   1,
		"two":   2,
		"three": 3,
	}
	t.Run("New", func(t *testing.T) {
		expected := &Output{
			Action:  "ONE",
			SentAt:  1234567654321,
			Sender:  "APPLICATION",
			Payload: payload,
		}
		actual := New(Output{
			Action:  "ONE",
			Sender:  "APPLICATION",
			Payload: payload,
		}, clockStub)
		assert.Equal(t, expected, actual)
	})

	t.Run("NewError", func(t *testing.T) {
		expected := &Output{
			Sender: "APPLICATION",
			SentAt: 1234567654321,
			Action: "ERROR",
			Payload: PayloadObject{
				"error": "something went wrong",
			},
		}
		actual := NewError("something went wrong", clockStub)
		assert.Equal(t, expected, actual)
	})

	t.Run("NewConnectionMsg", func(t *testing.T) {
		expected := &Output{
			Sender: "APPLICATION",
			SentAt: 1234567654321,
			Action: "ONE",
			Payload: PayloadObject{
				"connectionId": "123",
			},
		}
		actual := NewConnectionMsg("123", "ONE", clockStub)
		assert.Equal(t, expected, actual)
	})

	t.Run("NewPendingActionMsg", func(t *testing.T) {
		expected := &Output{
			Sender: "APPLICATION",
			SentAt: 1234567654321,
			Action: "PENDING_ACTION",
			Payload: PayloadObject{
				"pending": PayloadObject{
					"action": "ONE",
					"sender": "123",
				},
			},
		}
		actual := NewPendingActionMsg("123", "ONE", clockStub)
		assert.Equal(t, expected, actual)
	})

	t.Run("InitMsg", func(t *testing.T) {
		eventLoggerStub := _eventLoggerStub{}
		expected := &Output{
			Sender: "APPLICATION",
			SentAt: 1234567654321,
			Seed:   uint32(5),
			Action: "INIT",
			Payload: PayloadObject{
				"connectionId": "234",
				"eventLog": []Output{
					Output{
						Action: "ONE",
						SentAt: 0000000000001,
						Sender: "sender1",
					},
					Output{
						Action: "TWO",
						SentAt: 0000000000002,
						Sender: "sender1",
					},
				},
			},
		}
		actual := InitMsg("234", 5, eventLoggerStub, clockStub)
		assert.Equal(t, expected, actual)
	})
}
