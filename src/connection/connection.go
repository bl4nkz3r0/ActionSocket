package connection

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"

	"message"

	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
)

type Connection struct {
	Id     string
	Msg    chan *message.Output
	Ctx    context.Context
	socket *websocket.Conn
}

func CreateConnection(socket *websocket.Conn, ctx context.Context) *Connection {
	return &Connection{
		Id:     uuid.Must(uuid.NewV4()).String(),
		Msg:    make(chan *message.Output, 1),
		Ctx:    ctx,
		socket: socket,
	}
}

type sessionInterface interface {
	Broadcast(msg *message.Output)
	BroadcastAll(msg *message.Output)
	BroadcastOnConsensus(fromId string, input *message.Input, rawMessage []byte)
	BroadcastOnQuorum(fromId string, input *message.Input, rawMessage []byte, quorum int)
	BroadcastOnCollation(fromId string, input *message.Input, skipCount int)
	Send(to string, msg *message.Output)
	SendToMany(toIds []string, msg *message.Output)
}

func (c *Connection) Publish(session sessionInterface, cancel context.CancelFunc) {
	for {
		select {
		case <-c.Ctx.Done():
			return
		case message := <-c.Msg:
			err := c.socket.WriteJSON(message)
			if err != nil {
				log.Println("Socket Error:", err)
				cancel()
			}
		}
	}
}

func (c *Connection) Subscribe(session sessionInterface, cancel context.CancelFunc) {
	for {
		select {
		case <-c.Ctx.Done():
			return
		default:
			msgType, b, err := c.socket.ReadMessage()
			if err != nil {
				log.Println("Read error:", err)
				cancel()
			}
			if msgType == websocket.CloseMessage {
				cancel()
			}
			if msgType == websocket.TextMessage {
				c.handleMessage(session, b)
			}
		}
	}
}

func (c *Connection) handleMessage(session sessionInterface, b []byte) {
	buff := bytes.NewBuffer(b)
	dec := json.NewDecoder(buff)
	var input message.Input

	if err := dec.Decode(&input); err != nil {
		o := message.NewError(fmt.Sprintf("Could not decode JSON:  %v", err))
		go session.Send(c.Id, o)
		return
	}

	if input.Method == "" || input.Action == "" {
		o := message.NewError(fmt.Sprintf("Malformed Message: Message must have a Method and An Action (got method: %v action: %v)", input.Method, input.Action))
		go session.Send(c.Id, o)
		return
	}

	switch input.Method {
	case "COLLATE":
		session.BroadcastOnCollation(c.Id, &input, 0)
	case "COLLATE_AND_DROP_ONE":
		session.BroadcastOnCollation(c.Id, &input, 1)
	case "QUORUM":
		session.BroadcastOnQuorum(c.Id, &input, b, input.Quorum)
	case "CONSENSUS":
		session.BroadcastOnConsensus(c.Id, &input, b)
	case "BROADCAST":
		msg := input.ToOutput(c.Id)
		go session.BroadcastAll(msg)
	case "BROADCAST_OUT":
		msg := input.ToOutput(c.Id)
		go session.Broadcast(msg)
	default: // "DIRECT"
		msg := input.ToOutput(c.Id)
		if len(input.Addresses) > 1 {
			go session.SendToMany(input.Addresses, msg)
		} else {
			go session.Send(input.Addresses[0], msg)
		}
	}
}
