// +build mage

package main

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"

	"github.com/magefile/mage/mg"
)

var Default = Build

func Build() {
	log.Println("Building...")
	cmd := exec.Command("go", "build", "-o", "bin/server", "./src/server/server.go")
	var out bytes.Buffer
	var errout bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &errout
	err := cmd.Run()
	if err != nil {
		fmt.Print(errout.String())
	}
	fmt.Print(out.String())
}

func Run() error {
	log.Println("Running...")
	mg.Deps(Build)
	return exec.Command("server").Run()
}
